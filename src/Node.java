class Node
{
	// Trie supports lowercase English characters
	// so character size is 26 (a - z)
	int CHAR_SIZE = 26;

	boolean exist;		// true when node is a leaf node
	Node[] next;

	// Constructor
	Node() {
		next = new Node[CHAR_SIZE];
		exist = false;
	}
};